import React, { useState, createContext } from "react";


export const LoginContext = createContext();

export const LoginProvider = props => {
    const [username, setUser] = useState("");
    const [password, setPass] = useState("")
    
    

  
    return (
      <LoginContext.Provider value={[username, setUser,password, setPass
      ]}>
        {props.children}
      </LoginContext.Provider>
    );
  };