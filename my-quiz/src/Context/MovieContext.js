import React, { useState, createContext } from "react";

export const MovieContext = createContext();

export const MovieProvider = props => {
    const [daftarMovie, setdaftarMovie] = useState(null);
    const [iId,setId] = useState(null)
    const [iTitle, setTitle] = useState("")
    const [iDesc, setDesc] = useState("");
    const [iYear, setYear] = useState(2020);
    const [iDuration, setDuration] = useState(120);
    const [iGenre, setGenre] = useState("");
    const [iRating, setRating]=useState("");
    const [iImage,setImage] = useState("");
    const [search,setSearch] = useState("");
    const [username, setUser] = useState("");
    const [password, setPass] = useState("")
    

  
    return (
      <MovieContext.Provider value={[daftarMovie,setdaftarMovie,iId,setId,iTitle, setTitle,iDesc, setDesc,iYear, setYear,
        iDuration, setDuration, iGenre, setGenre, iRating, setRating,iImage,setImage,search,setSearch, username, setUser,password, setPass
      ]}>
        {props.children}
      </MovieContext.Provider>
    );
  };