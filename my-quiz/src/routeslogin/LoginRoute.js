import React, {useEffect,useContext} from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Nav from '../navbar/nav'
import Home from '../Home/index'
import About from '../about/about'
import Movie from '../Movie/Movie'
import Login from '../login/login'

const RoutesLogin = () =>{
    return(
        <Router>
        
        <Nav />
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
            {/*Tidak pakai end tag */}
            <Route exact path="/" component={Home}/>
           <Route path="/Movie">
            <Movie />
           </Route>
           <Route path="/about">
            <About />
           </Route>
            <Route path="/login">
            <Login />
            </Route>
        </Switch>
    </Router>
    )
    
};

export default RoutesLogin