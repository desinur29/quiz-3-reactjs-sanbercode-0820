import React, {useContext, useState, useEffect} from 'react'
import { useHistory } from 'react-router-dom';
import {MovieContext} from '../Context/MovieContext'


const Login = (props) =>{

    const [username, setUser,password, setPass
    ] = useContext(MovieContext);
    const history = useHistory();
    

    //this.setState({ error: false });

    const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "user":
          {
            setUser(event.target.value);
            break
          }
          case "pass":
          {
            setPass(event.target.value);
            break
          }
          
        default:
          {break;}
        }
      }

    const cek = (event) =>{
        event.preventDefault();
           if(username == "admin" && password == "admin"){
               var fine = history.push("../routes/Routes");
               if (fine === true){
                console.log("login berhasil")
               }
            }else{
                console.log("gagal")
            }
    } 
        
    return(
        <>
            <div id="index">
                <section style={{height:"500px"}}>
                <h1>Form Login</h1>
                <label>Username</label>
                <input name="user" type="text" value={username} onChange={handleChange}/>
                <label>password :</label>
                <input name="pass" type="password" value={password} onChange={handleChange}/>
                <center><button onClick={cek}>login</button></center>
                </section>
                <footer>
                <h5>copyright &copy; 2020 by Sanbercode</h5>
            </footer>
            </div>
            
        </>
    )
}

export default Login