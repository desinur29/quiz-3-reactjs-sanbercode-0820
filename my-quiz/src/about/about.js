import React from 'react';


const About = () =>{

    return(
        <>
            <div id="index">
            <section>
            <div id="data-peserta">
                <h1>Data Peserta Sanbercode ReactJs Bootcamp</h1>
                <ol>
                    <li><b>Nama : </b>Desi Nurhasannah</li>
                    <li><b> Email :</b> desinurhasanah20@gmail.com</li>
                    <li><b>Sistem Operasi yang digunakan :</b>Windows 10</li>
                    <li><b>Akun Gitlab :</b> @desinur29 / desinurhasanah20@gmail.com</li>
                    <li><b>Akun Telegram :</b>@desinur_29 / 082213186773</li>
                </ol>
            </div>
            </section>
            <footer>
                <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
        </div>
        </>
    )
}

export default About