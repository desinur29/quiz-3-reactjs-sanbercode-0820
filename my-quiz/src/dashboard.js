import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './routes/Routes'
import {MovieProvider} from './Context/MovieContext'



function Dashboard() {
    return (
        <MovieProvider>
            <Routes/>
        </MovieProvider>
      
       
    );
  }
  
  export default Dashboard;