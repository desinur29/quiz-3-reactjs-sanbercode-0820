import React, {useContext, useState, useEffect} from 'react'
import axios from 'axios'
import {MovieContext} from '../Context/MovieContext'

const MovieList = () =>{
    const [daftarMovie,setdaftarMovie,iId,setId,iTitle, setTitle,iDesc, setDesc,iYear, setYear,
        iDuration, setDuration, iGenre, setGenre, iRating, setRating,iImage,setImage,search,setSearch] = useContext(MovieContext);
    
    const handleChangeSearch = (event) =>{
        setSearch(event.target.value)
    }

    const handleSearch = (event) =>{
        event.preventDefault();
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
            if(search !== ""){
                let pencarian = daftarMovie.filter(el=>el.title === search ||el.description === search||el.year === parseInt(search)||el.duration === parseInt(search)||el.genre === search||el.rating === parseInt(search))
                setdaftarMovie(pencarian)
                setSearch("")
            }else{
                setdaftarMovie(res.data);
            }
                
        })
        
                
    }
    
    
        


    const editMovie = (event) =>{
        let idMovie = parseInt(event.target.value);
        let movie = daftarMovie.find(el=>el.id === idMovie)
 
        setId(movie.id)
        setTitle(movie.title)
        setDesc(movie.description)
        setYear(movie.year)
        setDuration(movie.duration)
        setGenre(movie.genre)
        setRating(movie.rating)
        setImage(movie.image_url)
        
    }

    const deleteMovie =(event) =>{
        let idMovie = parseInt(event.target.value)
         axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
         .then(res => {
         let newdaftarMovie = daftarMovie.filter(el=> el.id !== idMovie)
         setdaftarMovie(newdaftarMovie)
         setId(null)
         setTitle("")
         setDesc("")
         setYear(2020)
         setDuration(120);
         setGenre("")
         setRating("");
         setImage("")
        })
    }

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
            if(daftarMovie === null){
                setdaftarMovie(res.data);
                console.log(res.data)
                
            }
                
        })
      },[daftarMovie]);

      return(
          <>
                    <div style={{ width:"35%", margin:"0 auto"}}>
                        <input style={{float:"left" , margin:"10px"}}name="search" type="text" value={search} onChange={handleChangeSearch} />
                        <button style={{float:"right", margin:"10px"}}onClick={handleSearch}>Search</button>
                    </div>
                    <table>
                        <thead>
                            <th>No</th>
                            <th>Title</th>
                            <th> Description</th>
                            <th>Year</th>
                            <th>Duration</th>
                            <th>Genre</th>
                            <th>Rating</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            {daftarMovie !== null && daftarMovie.map((el,index) => {
                                        return(
                                    <tr key={el.id}>
                                        <td>{index + 1}</td>
                                        <td>{el.title}</td>
                                        <td>{el.description.substr(0,20)} ....</td>
                                        <td>{el.year}</td>
                                        <td>{el.duration}</td>
                                        <td>{el.genre}</td>
                                        <td>{el.rating}</td>
                                        <td>
                                            <button onClick={editMovie} value={el.id}>Edit</button>
                                            <button onClick={deleteMovie} value={el.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                
          </>
      )


}

export default MovieList