import React, {useContext, useEffect} from 'react'
import {MovieProvider} from '../Context/MovieContext'
import MovieList from './MovieList'
import MovieForm from './MovieForm'


const Movie = () =>{
    return(
        <div id="index">
            <section>
                    <MovieList />
                    <MovieForm />
            </section>
            <footer>
                <h5>copyright &copy; 2020 by Sanbercode</h5>
            </footer>
        </div>
       
       
    )
}

export default Movie