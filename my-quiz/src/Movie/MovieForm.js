import React, {useContext, useEffect, useState} from 'react'
import axios from 'axios'
import {MovieContext} from '../Context/MovieContext'

const MovieForm = () =>{
    const [daftarMovie,setdaftarMovie,iId,setId,iTitle, setTitle,iDesc, setDesc,iYear, setYear,
        iDuration, setDuration, iGenre, setGenre, iRating, setRating,iImage,setImage] = useContext(MovieContext);
    

    const handleChange = (event) =>{
            let typeOfInput = event.target.name
        
            switch (typeOfInput){
              case "title":
              {
                setTitle(event.target.value);
                break
              }
              case "desc":
              {
                setDesc(event.target.value);
                break
              }
              case "year":
              {
                setYear(event.target.value);
                  break
              }
              case "duration":
              {
                
                setDuration(event.target.value);
                  break
              }
              case "genre":
              {
                setGenre (event.target.value);
                  break
              }
              case "rating":
              {
                setRating(event.target.value);
                  break
              }
              case "img":
              {
                setImage(event.target.value);
                  break
              }
            default:
              {break;}
            }
          }
        
    
    
        const handleSubmit = (event) => {
            event.preventDefault();
                if(iId === null){
                    axios.post(`http://backendexample.sanbercloud.com/api/movies`,{
                        title:iTitle, 
                        description:iDesc,
                        year:iYear,
                        duration:iDuration,
                        genre:iGenre,
                        rating:iRating,
                        image_url:iImage
                    })
                    .then(res => {
                        let data = res.data;
                        console.log(data.id);
                        setdaftarMovie([...daftarMovie, {
                            id: data.id, 
                            title : data.title, 
                            description:data.description, 
                            year:data.year,
                            duration:data.duration,
                            genre:data.genre,
                            rating:data.rating,
                            image_url:data.image_url
                            }])
                        setId(null)
                        setTitle("")
                        setDesc("")
                        setYear(2020)
                        setDuration(120);
                        setGenre("")
                        setRating("");
                        setImage("")
                        })
                        console.log("berhasil input")
                }else{
                    axios.put(`http://backendexample.sanbercloud.com/api/movies/${iId}`,{ 
                        title:iTitle, 
                        description:iDesc,
                        year:iYear,
                        duration:iDuration,
                        genre:iGenre,
                        rating:iRating,
                        image_url:iImage
                    })
                    .then(res => {
                        let data = res.data;
                        let newdaftarMovie = daftarMovie.map(item =>{
                            if(item.id === iId){
                                item.title = data.title 
                                item.description = data.description
                                item.year =data.year
                                item.duration =data.duration
                                item.genre = data.genre
                                item.rating = data.rating
                                item.image_url=data.image_url
                            }
                            return item
                            })
                            setdaftarMovie(newdaftarMovie);
                            setId(null)
                            setTitle("")
                            setDesc("")
                            setYear(2020)
                            setDuration(120);
                            setGenre("")
                            setRating("");
                            setImage("")
                })
            }
        }


    return(
        <>
        <h1>Movies Form</h1>
        <div style={{width: "60%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
            <form onSubmit={handleSubmit}>
            <label style={{float:"left"}} >Title :</label>
            <input style={{float:"right"}}  required="required" name="title" type="text" value={iTitle} onChange={handleChange} />
            <br />
            <br />
            <label style={{float:"left"}}>Description :</label>
            <textarea style={{float: "right"}} name="desc" value={iDesc} onChange={handleChange}></textarea>
            <br />
            <br />
            <br />
            <label style={{float:"left"}}>Year :</label>
            <input style={{float:"right"}} required="required" name="year" type="number" value={iYear} onChange={handleChange} min="1980" />
            <br />
            <br />
            <label style={{float:"left"}}>Duration (minute) :</label>
            <input style={{float:"right"}} required="required" name="duration" type="number" value={iDuration} onChange={handleChange} />
            <br />
            <br />
            <label style={{float:"left"}}>Genre :</label>
            <input style={{float:"right"}} required="required" name="genre" type="text" value={iGenre} onChange={handleChange} />
            <br />
            <br />
            <label style={{float:"left"}}>Rating :</label>
            <input style={{float:"right"}} required="required" name="rating" type="number" value={iRating} onChange={handleChange} min="0" max="10"/>
            <br />
            <br />
            <label style={{float:"left"}}>Image Url :</label>
            <textarea style={{float:"right"}} required="required" name="img" type="text" value={iImage} onChange={handleChange}/>
            <br />
            <br />
            <br />
            <br />
            <button style={{float:"right", marginBottom:"20px"}} >submit</button>
            <br />
            <br />
        </form>
            </div>
        
        </div>
         </>  

        
        
    )
}

export default MovieForm