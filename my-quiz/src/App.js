import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './routes/Routes'
import {MovieProvider} from './Context/MovieContext'
import {LoginProvider} from './Context/LoginContext'
import LoginRoutes from './routeslogin/LoginRoute'

function App() {
  return (
             <MovieProvider>
              <Routes />
             </MovieProvider>
  );
}

export default App;
