import React from 'react';
import {Link, BrowserRouter, useHistory} from "react-router-dom";
import logo from '../public/img/logo.png'

const Navbar = () =>{
    const history = useHistory();

    const handleLogout = ()=>{
        alert('blum bisa login dan logout')
    }

    return(
        <>
            <header>
                <nav class="nav">
                    <div class="nav-kiri">
                    <img id="logo" src={logo} width="200px"/>
                    </div>
                    <div class="nav-kanan">
                    <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/About">About</Link></li>
                    <li><Link to="/Movie">Movie</Link></li>
                    <li><Link onClick={handleLogout}>Logout</Link></li>
                    </ul>
                    </div>
                </nav>
            </header>
        </>

    )
}

export default Navbar
