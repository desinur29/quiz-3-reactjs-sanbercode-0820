import React from 'react';
import {Link} from "react-router-dom";
import logo from '../public/img/logo.png'

const Nav = () =>{

    return(
        <>
            <header>
                <nav class="nav">
                    <div class="nav-kiri">
                    <img id="logo" src={logo} width="200px"/>
                    </div>
                    <div class="nav-kanan">
                    <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/About">About</Link></li>
                    <li><Link to="/login">Login</Link></li>
                    </ul>
                    </div>
                </nav>
            </header>
        </>

    )
}

export default Nav
