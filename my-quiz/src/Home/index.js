import React, { useContext, useEffect } from 'react';
import axios from 'axios'
import {MovieContext} from '../Context/MovieContext'

const Home = () =>{

    const [daftarMovie,setdaftarMovie] = useContext(MovieContext);

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
            if(daftarMovie === null){
                setdaftarMovie(res.data);
                console.log(res.data)
            }
                
        })
      },[daftarMovie]);

    return(
        <>
        <div id="index">
        <section>
            <h1>Daftar Film Terbaik</h1>
            <div id="article-list">
                {daftarMovie !== null && daftarMovie.map(el =>{
                     return(
                        <div class="list">
                            <h2>{el.title}</h2>
                            <div class="bio">
                                <div class="container"> 
                                <img src={el.image_url} />
                                </div>
                                <div class="keterangan">
                                    <h4>Rating : {el.rating}</h4>
                                    <h4>Durasi : {el.durtation}</h4>
                                    <h4>Genre : {el.genre}</h4>
                                </div>
                            </div>
                            <div style={{textAlign:"justify"}}>
                            <p>
                                <bold>deskripsi :</bold> {el.description}
                            </p>
                            </div>
                            
                            <hr />
                        </div>
                    )
                })} 
            </div>
        </section>
        <footer>
                <h5>copyright &copy; 2020 by Sanbercode</h5>
            </footer>
        </div> 
    </>
        
    )
}

export default Home;